Dynamic Kickstart Generator
===========================
Nodejs express app that takes URL parameters to dynamically generate kickstart file

**Note**: this currently supports a basic minimal install of CentOS 7, Ubuntu 16.04/14.04, and Atomic (CentOS) Linux 7

Use:
====
Edit config.yml in config directory

NPM install:
```
npm install
```

Start Node:
```
node server.js
```

API Examples to Generate ks.cfg for kickstarts:
=====================================
Generate Ubuntu 16.04:
```
curl -XGET 'http://localhost:3000/ks?os=ubuntu&version=16.04
```

Generate CentOS 7 (Build 1511) with static IP of 192.168.1.5
```
curl -XGET 'http://localhost:3000/ks?os=centos&version=7_1511&ip=192.168.1.5
```

Basic partitioning for Ubuntu 14.04 with IP of 192.168.1.65:
```
curl -XGET 'http://localhost:3000/ks?os=ubuntu&version=14.04&ip=192.168.1.65&part\[0\]=/boot&part\[0\]=ext4&part\[0\]=512&part\[1\]=/&part\[1\]=ext4&part\[1\]=20480&part\[1\]=--grow&part\[2\]=swap&part\[2\]=2048'
```
... generates the following partitioning scheme:
```
bootloader --location=mbr
clearpart --all --initlabel

part /boot --fstype=ext4 --size=512
part / --fstype=ext4 --size=20480 --grow
part swap --size=2048
```

License:
========
Copyright [2016] [John W. Reed II]
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
```
http://www.apache.org/licenses/LICENSE-2.0
```
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
