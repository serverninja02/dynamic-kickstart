FROM node:6.3.1

MAINTAINER serverninja@gmail.com

ARG APPDIR=/usr/src/app
ARG APPORT=9000

RUN mkdir -pv ${APPDIR}/dynamic-kickstart && \
  apt-get update && \
  apt-get -y install git

ADD . ${APPDIR}/dynamic-kickstart/

RUN cd ${APPDIR}/dynamic-kickstart && \
  npm install

WORKDIR ${APPDIR}/dynamic-kickstart

CMD [ "node", "server.js" ]
