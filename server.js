var express = require('express');
var fs = require('fs');
var config = require('js-yaml');
var app = express();

app_conf = config.safeLoad(fs.readFileSync(__dirname + '/config/config.yml', 'utf8'));

app.set('view engine', 'ejs');

app.get('/ks', function(req, res) {
    console.log("Request Parameters: " + JSON.stringify(req.query));
    ksConfig = {}
    query = req.query

    if (query.os) {

      if (query.part) {
        ksConfig.part = query.part
      } else {
        ksConfig.part = false;
      }
  
      if (query.lvm) {
        ksConfig.lvm = true;
      } else {
        ksConfig.lvm = false;
      }
  
      if (query.hostname) {
        ksConfig.hostname == query.hostname;
      } else {
        ksConfig.hostname == 'kickseed';
      }

      ksConfig.version = query.version
      ksConfig.os = query.os
      os_defaults = app_conf.defaults[query.os]

      ksConfig.install_type = os_defaults.install_type

      ksConfig.ip = query.ip

      //Static networking assignments from config
      ksConfig.netmask = app_conf.network.netmask
      ksConfig.gateway = app_conf.network.gateway
      ksConfig.nameservers = app_conf.network.nameservers
  
      //Static http server assignments from config
      ksConfig.http_host = app_conf.http.http_host
      ksConfig.http_port = app_conf.http.http_port
      ksConfig.http_proto = app_conf.http.http_proto
  
      res.render('pages/ks', ksConfig);
    } else {
      strErr = "ERROR: Need to specify os in parameters list"
      console.log(strErr)
      res.send(strErr)
   }
});

app.listen(3000, function () {
  console.log('dynamic-kickstart listening on port 3000');
});
